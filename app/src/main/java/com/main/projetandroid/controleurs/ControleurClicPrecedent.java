package com.main.projetandroid.controleurs;

import android.view.View;
import android.widget.ImageButton;

import com.main.projetandroid.modele.FiltrePage;
import com.main.projetandroid.recherche.CallbackRecherche;
import com.main.projetandroid.recherche.Recherche;

/**
 * Contrôle le bouton suivant.
 */
public class ControleurClicPrecedent extends Controleur implements View.OnClickListener {
    protected final Recherche recherche;
    protected final FiltrePage page;
    protected final CallbackRecherche callback;
    protected final ImageButton boutonPrecedent;
    protected final ImageButton boutonAvant;

    /**
     * Constructeur de la classe.
     *
     * @param recherche la recherche à lancer
     * @param page le filtre qui contient la page
     * @param cbRecherche la callback à appeler à la fin de la recherche
     * @param precedent le bouton « précédent »
     * @param suivant le bouton « suivant »
     */
    public ControleurClicPrecedent(Recherche recherche, FiltrePage page, CallbackRecherche cbRecherche, ImageButton precedent, ImageButton suivant) {
        this.recherche = recherche;
        this.page = page;
        this.callback = cbRecherche;
        this.boutonPrecedent = precedent;
        this.boutonAvant = suivant;
    }


    @Override
    public void onClick(View v) {
        cacherClavier(v);

        page.precedente();
        boutonAvant.setVisibility(View.VISIBLE);

        if (page.valeur() == FiltrePage.PREMIERE_PAGE)
            boutonPrecedent.setVisibility(View.GONE);

        recherche.lancer(callback);
    }
}
