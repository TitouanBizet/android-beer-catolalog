package com.main.projetandroid.controleurs;

import android.view.View;
import com.main.projetandroid.adaptateurs.AdaptateurFiltres;
import com.main.projetandroid.adaptateurs.AdaptateurSpinnerFiltres;
import com.main.projetandroid.modele.FiltreModifiable;

/**
 * Contrôleur de clic sur le bouton supprimer d’un filtre. Au clic, retire le filtre du Viewgroup et
 * l’ajoute au spinner de sélection.
 */
public class ControleurClicSupprimer extends Controleur implements View.OnClickListener {
    protected final FiltreModifiable<?> filtre;
    protected final AdaptateurFiltres adaptateurFiltres;
    protected final AdaptateurSpinnerFiltres adaptateurSpinnerFiltres;

    public ControleurClicSupprimer(FiltreModifiable<?> filtre, AdaptateurFiltres adaptateurFiltres, AdaptateurSpinnerFiltres adaptateurSpinnerFiltres) {
        this.filtre = filtre;
        this.adaptateurFiltres = adaptateurFiltres;
        this.adaptateurSpinnerFiltres = adaptateurSpinnerFiltres;
    }

    @Override
    public void onClick(View v) {
        cacherClavier(v);
        adaptateurFiltres.remove(filtre);
        adaptateurSpinnerFiltres.add(filtre.nom(v.getContext()));
    }
}
