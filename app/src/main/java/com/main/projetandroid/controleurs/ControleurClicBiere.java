package com.main.projetandroid.controleurs;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;

import com.main.projetandroid.ActiviteDetails;

/**
 * Controleur de clic sur une liste de bières. Cliquer sur une bière lance une activité qui montre
 * ses détails.
 */
public class ControleurClicBiere implements AdapterView.OnItemClickListener {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Context contexte = parent.getContext();

        Intent intent = new Intent(contexte, ActiviteDetails.class);
        intent.putExtra("biere", (Parcelable) parent.getItemAtPosition(position));

        contexte.startActivity(intent);
    }
}
