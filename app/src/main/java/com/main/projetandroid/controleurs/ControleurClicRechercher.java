package com.main.projetandroid.controleurs;

import android.view.View;
import android.widget.ImageButton;

import com.main.projetandroid.modele.FiltreNombreResultats;
import com.main.projetandroid.modele.FiltrePage;
import com.main.projetandroid.recherche.CallbackRecherche;
import com.main.projetandroid.recherche.CallbackVerification;
import com.main.projetandroid.recherche.Recherche;

/**
 * Contrôleur lançant une recherche de bières en fonction de filtres et les affichant dans une
 * ViewGroup au clic.
 */
public class ControleurClicRechercher extends Controleur implements View.OnClickListener {
    protected final Recherche recherche;
    protected final FiltreNombreResultats nbResultats;
    protected final FiltrePage page;
    protected final CallbackRecherche callbackRecherche;
    protected final CallbackVerification callbackVerif;
    protected final ImageButton precedent;
    protected final ImageButton suivant;

    /**
     * Constructeur de la classe.
     *
     * @param recherche la recherche à lancer
     * @param nbResultats le filtre du nombre de résultats sur une page
     * @param page le filtre qui contient la page
     * @param cbRecherche la callback à appeler à la fin de la recherche
     * @param cbVerif la callback à appeler lors de la vérification de la page suivante
     * @param precedent le bouton « précédent »
     * @param suivant le bouton « suivant »
     */
    public ControleurClicRechercher(Recherche recherche, FiltreNombreResultats nbResultats, FiltrePage page, CallbackRecherche cbRecherche, CallbackVerification cbVerif, ImageButton precedent, ImageButton suivant) {
        this.recherche = recherche;
        this.nbResultats = nbResultats;
        this.page = page;
        this.callbackRecherche = cbRecherche;
        this.callbackVerif = cbVerif;
        this.precedent = precedent;
        this.suivant = suivant;
    }


    @Override
    public void onClick(View v) {
        if (recherche.parametresValides()) {
            cacherClavier(v);

            page.reinitialiser();
            recherche.maj();
            recherche.lancer(callbackRecherche);

            precedent.setVisibility(View.INVISIBLE);

            // On vérifie s’il y a des résultats dans la page suivante
            callbackVerif.setNbResults(nbResultats.valeur());
            callbackVerif.setPage(page.valeur());
            recherche.majSuivant(callbackVerif);
        }
    }
}
