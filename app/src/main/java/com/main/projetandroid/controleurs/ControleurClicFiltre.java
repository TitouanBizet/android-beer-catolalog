package com.main.projetandroid.controleurs;

import android.view.View;
import android.widget.AdapterView;

import com.main.projetandroid.adaptateurs.AdaptateurFiltres;
import com.main.projetandroid.adaptateurs.AdaptateurSpinnerFiltres;
import com.main.projetandroid.modele.Filtre;
import com.main.projetandroid.modele.FiltreModifiable;

import java.util.Map;

/**
 * Contrôleur de clic sur un filtre dans un spinner. Au clic, le filtre est retiré du spinner et
 * ajouté à un ViewGroup.
 */
public class ControleurClicFiltre implements AdapterView.OnItemSelectedListener {
    protected final AdaptateurFiltres adaptateurFiltres;
    protected final AdaptateurSpinnerFiltres adaptateurSpinnerFiltres;

    public ControleurClicFiltre(AdaptateurFiltres adaptateurFiltres, AdaptateurSpinnerFiltres adaptateurSpinnerFiltres) {
        this.adaptateurFiltres = adaptateurFiltres;
        this.adaptateurSpinnerFiltres = adaptateurSpinnerFiltres;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String nom_filtre = (String) parent.getItemAtPosition(position);
        Map<String, FiltreModifiable<?>> filtres = Filtre.par_defaut();

        if (filtres.containsKey(nom_filtre)) {
            adaptateurFiltres.add(filtres.get(nom_filtre));
            adaptateurSpinnerFiltres.remove(nom_filtre);
            parent.setSelection(0);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
