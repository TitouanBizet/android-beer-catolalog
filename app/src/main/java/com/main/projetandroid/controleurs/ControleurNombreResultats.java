package com.main.projetandroid.controleurs;

import android.widget.SeekBar;
import android.widget.TextView;

import com.main.projetandroid.modele.FiltreNombreResultats;

import java.security.InvalidParameterException;

/**
 * Contrôle une barre de progression pour changer le nombre de résultats à afficher.
 */
public class ControleurNombreResultats implements SeekBar.OnSeekBarChangeListener {
    protected SeekBar barre;
    protected FiltreNombreResultats nombreRecherches;
    protected TextView afficheur;

    /**
     * Crée un contrôleur du nombre de résultats.
     *
     * @param barre la barre contrôlant le nombre de résultats
     * @param nombreRecherches un objet de type Integer correspondant au nombre de résultats à
     *                         afficher.
     */
    public ControleurNombreResultats(SeekBar barre, FiltreNombreResultats nombreRecherches, TextView afficheur) {
        if (barre == null || nombreRecherches == null)
            throw new InvalidParameterException();

        this.barre = barre;
        this.nombreRecherches = nombreRecherches;
        this.afficheur = afficheur;

        mettreBarreA(FiltreNombreResultats.VALEUR_PAR_DEFAUT);
        this.barre.setMax(FiltreNombreResultats.VALEUR_MAX / 5 - 1);
        this.afficheur.setText(String.valueOf(valeurDeLaBarre()));
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        int valeur = valeurDeLaBarre();

        nombreRecherches.changerValeur(valeur);
        afficheur.setText(String.valueOf(valeur));
    }

    /**
     * Convertit la valeur choisie par l’utilisateur.
     *
     * @return le nombre de résultats que l’utilisateur veut.
     */
    protected int valeurDeLaBarre() {
        return (barre.getProgress() + 1) * 5;
    }

    /**
     * Convertit la valeur et met à jour la barre.
     *
     * @param valeur le nombre de résultats désiré.
     */
    protected void mettreBarreA(int valeur) {
        barre.setProgress(valeur / 5 - 1);
    }
}
