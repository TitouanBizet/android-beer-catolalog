package com.main.projetandroid.adaptateurs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.main.projetandroid.R;
import com.main.projetandroid.modele.Biere;
import com.main.projetandroid.modele.Bieres;
import com.squareup.picasso.Picasso;

/**
 * Adapte une liste de bières dans un ViewGroup où figurent leur nom et leur photo.
 */
public class AdaptateurBieres extends ArrayAdapter<Biere> {
    LayoutInflater inflater;

    public AdaptateurBieres(@NonNull Context context) {
        super(context, R.layout.biere_layout, new Bieres());
        inflater = LayoutInflater.from(context);
    }

    public AdaptateurBieres(@NonNull Context context, @NonNull Bieres bieres) {
        super(context, R.layout.biere_layout, bieres);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        ViewHolder vh;

        if (view == null) {
            view = inflater.inflate(R.layout.biere_layout, parent, false);

            vh = new ViewHolder();
            vh.icone = view.findViewById(R.id.icone);
            vh.etiquette = view.findViewById(R.id.etiquette);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) view.getTag();
        }

        Biere biere = getItem(position);

        Picasso.get().load(biere.url_image).into(vh.icone);
        vh.etiquette.setText(biere.nom);

        return view;
    }

    private static class ViewHolder {
        ImageView icone;
        TextView etiquette;
    }
}
