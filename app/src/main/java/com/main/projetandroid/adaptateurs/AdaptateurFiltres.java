package com.main.projetandroid.adaptateurs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.main.projetandroid.R;
import com.main.projetandroid.controleurs.ControleurClicSupprimer;
import com.main.projetandroid.controleurs.ControleurValeurFiltre;
import com.main.projetandroid.modele.FiltreModifiable;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapte une liste de filtres sélectionnés à un ViewGroup l’on peut changer leur valeur ou les
 * supprimer.
 */
public class AdaptateurFiltres extends ArrayAdapter<FiltreModifiable<?>> {
    protected final LayoutInflater inflater;
    protected final AdaptateurSpinnerFiltres adaptateurSpinner;

    public AdaptateurFiltres(@NonNull Context context, @NonNull List<FiltreModifiable<?>> filtres, AdaptateurSpinnerFiltres adaptateurSpinner) {
        super(context, R.layout.filtre_layout, filtres);
        this.inflater = LayoutInflater.from(context);
        this.adaptateurSpinner = adaptateurSpinner;
    }

    public AdaptateurFiltres(@NonNull Context contexte, AdaptateurSpinnerFiltres adaptateurSpinner) {
        this(contexte, new ArrayList<>(), adaptateurSpinner);
    }
    
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        AdaptateurFiltres.ViewHolder vh;
        FiltreModifiable<?> filtre = getItem(position);
        int tag_filtre = filtre.hashCode();

        if (view == null || view.getTag(tag_filtre) == null) {
            view = inflater.inflate(R.layout.filtre_layout, parent, false);

            vh = new AdaptateurFiltres.ViewHolder();

            vh.nom_filtre = view.findViewById(R.id.nom_filtre);
            vh.valeur_filtre = view.findViewById(R.id.valeur_filtre);
            vh.supprimer = view.findViewById(R.id.supprimer);

            view.setTag(tag_filtre, vh);
        } else {
             vh = (AdaptateurFiltres.ViewHolder) view.getTag(tag_filtre);
        }


        vh.nom_filtre.setText(filtre.nom(getContext()));

        // Évite d’avoir plusieurs contrôleurs sur la même vue.
        if (vh.controleurValeur != null)
            vh.valeur_filtre.removeTextChangedListener(vh.controleurValeur);

        vh.controleurValeur = new ControleurValeurFiltre(filtre, vh.valeur_filtre);
        vh.valeur_filtre.addTextChangedListener(vh.controleurValeur);

        vh.supprimer.setOnClickListener(new ControleurClicSupprimer(filtre, this, adaptateurSpinner));

        return view;
    }

    private static class ViewHolder {
        TextView nom_filtre;
        EditText valeur_filtre;
        ImageButton supprimer;

        ControleurValeurFiltre controleurValeur;
    }
}
