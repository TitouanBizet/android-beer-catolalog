package com.main.projetandroid;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.main.projetandroid.adaptateurs.*;
import com.main.projetandroid.controleurs.ControleurClicBiere;
import com.main.projetandroid.controleurs.ControleurClicFiltre;
import com.main.projetandroid.controleurs.ControleurClicPrecedent;
import com.main.projetandroid.controleurs.ControleurClicRechercher;
import com.main.projetandroid.controleurs.ControleurClicSuivant;
import com.main.projetandroid.controleurs.ControleurNombreResultats;
import com.main.projetandroid.modele.FiltreNombreResultats;
import com.main.projetandroid.modele.FiltrePage;
import com.main.projetandroid.recherche.CallbackRecherche;
import com.main.projetandroid.recherche.CallbackVerification;
import com.main.projetandroid.recherche.Recherche;
import com.main.projetandroid.vue.VueMessage;

/**
 * Activité principale. On peut y ajouter et configurer des filtres, puis lancer une recherche qui
 * donne une liste de bières provenant de punkapi.com. Au clic sur l’une de ces bières, l’activité
 * ActiviteDetails est lancée.
 */
public class ActivitePrincipale extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activite_principale);

        // Récupération des élements que l’on va paramétrer
        Spinner choixFiltres = findViewById(R.id.choixFiltre);
        ImageButton precedent = findViewById(R.id.precedent);
        ImageButton suivant = findViewById(R.id.suivant);
        VueMessage message = findViewById(R.id.message);
        ListView listeBieres = findViewById(R.id.listeBieres);
        SeekBar barre = findViewById(R.id.choixNombre);
        ListView listeFiltres = findViewById(R.id.listeFiltres);
        TextView compteur = findViewById(R.id.compteur);
        Button bouton = findViewById(R.id.rechercher);

        // Création des adaptateurs
        AdaptateurSpinnerFiltres adaptateurChoix = new AdaptateurSpinnerFiltres(choixFiltres);
        AdaptateurFiltres adaptateurFiltres = new AdaptateurFiltres(this, adaptateurChoix);
        AdaptateurBieres adaptateurBieres = new AdaptateurBieres(this);

        // Ces filtres sont uniques puisqu’ils sont toujours paramétrés
        FiltreNombreResultats nbResultats = new FiltreNombreResultats();
        FiltrePage page = new FiltrePage();

        // Ces classes s’occupent des connexions à l’API et de leurs effets sur l’application
        CallbackRecherche cbRecherche = new CallbackRecherche(adaptateurBieres, message, listeBieres);
        CallbackVerification cbVerif = new CallbackVerification(adaptateurBieres, message, suivant);
        Recherche recherche = new Recherche(adaptateurFiltres, nbResultats, page, precedent);

        // Ajouts des adaptateurs
        choixFiltres.setAdapter(adaptateurChoix);
        listeFiltres.setAdapter(adaptateurFiltres);
        listeBieres.setAdapter(adaptateurBieres);

        // Ajout des contrôleurs
        precedent.setOnClickListener(new ControleurClicPrecedent(recherche, page, cbRecherche, precedent, suivant));
        suivant.setOnClickListener(new ControleurClicSuivant(recherche, nbResultats, page, cbRecherche, cbVerif, precedent, suivant));
        choixFiltres.setOnItemSelectedListener(new ControleurClicFiltre(adaptateurFiltres, adaptateurChoix));
        barre.setOnSeekBarChangeListener(new ControleurNombreResultats(barre, nbResultats, compteur));
        bouton.setOnClickListener(new ControleurClicRechercher(recherche, nbResultats, page, cbRecherche, cbVerif, precedent, suivant));
        listeBieres.setOnItemClickListener(new ControleurClicBiere());
    }
}