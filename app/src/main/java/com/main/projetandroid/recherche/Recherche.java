package com.main.projetandroid.recherche;

import android.content.Context;
import android.widget.ImageButton;

import com.google.gson.JsonArray;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.main.projetandroid.adaptateurs.AdaptateurFiltres;
import com.main.projetandroid.modele.Filtre;
import com.main.projetandroid.modele.FiltreNombreResultats;
import com.main.projetandroid.modele.FiltrePage;

/**
 * Une classe permettant de paramétrer puis de lancer une recherche puis de la lancer.
 */
public class Recherche {
    protected final int PREMIERE_PAGE = 1;
    protected final String URL_API = "https://api.punkapi.com/v2/beers";

    protected final AdaptateurFiltres filtres;
    protected final FiltreNombreResultats nbResultats;
    protected final FiltrePage page;

    protected final ImageButton boutonArriere;
    protected String url;

    /**
     * Constructeur de la classe.
     * @param filtres les filtres utilisés.
     * @param nbResultats le nombre de résultats à demander.
     * @param page le filtre de la page à afficher
     * @param boutonArriere le bouton précédent de la vue principale
     */
    public Recherche(AdaptateurFiltres filtres, FiltreNombreResultats nbResultats, FiltrePage page, ImageButton boutonArriere) {
        this.filtres = filtres;
        this.nbResultats = nbResultats;
        this.page = page;
        this.boutonArriere = boutonArriere;

        maj();
    }

    /**
     * Lance une recherche en fonction des filtres et du nombre de paramètres.
     *
     * @param callback L’action à réaliser lorsque la recherche est finie.
     */
    public void lancer(FutureCallback<JsonArray> callback) {
        final Context contexte = filtres.getContext();
        if (parametresValides()) {
            final String url_complete = url + "&" + page.url();
            Ion.with(contexte).load(url_complete).asJsonArray().setCallback(callback);
        } else {
            callback.onCompleted(new ExceptionParametres(), null);
        }
    }

    /**
     * Met à jour l’affichage du bouton suivant.
     */
    public void majSuivant(CallbackVerification callback) {
        final Context contexte = filtres.getContext();
        final String url_complete = url.replace("?" + nbResultats.url(), "?");
        Ion.with(contexte).load(url_complete).asJsonArray().setCallback(callback);
    }

    /**
     * Détermine si les valeurs des filtres définies par l’utilisateur sont valides.
     *
     * @return un booléen.
     */
    public boolean parametresValides() {
        boolean parametresValides = true;

        for (int i = 0; i < filtres.getCount(); i++) {
            if (!filtres.getItem(i).estValide()) {
                parametresValides = false;
                break;
            }
        }

        return parametresValides;
    }

    /**
     * Met à jour l’URL à utiliser pour la recherche en ajoutant les paramètres valides.
     */
    public void maj() {
        final StringBuilder parametres = new StringBuilder("?" + nbResultats.url());

        Filtre<?> filtre;
        for (int i = 0; i < filtres.getCount(); i++) {
            filtre = filtres.getItem(i);

            if (filtre.estValide())
                parametres.append("&").append(filtre.url());
        }

        url = URL_API + parametres;
    }
}
