package com.main.projetandroid.recherche;

import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.google.gson.JsonArray;
import com.koushikdutta.async.future.FutureCallback;
import com.main.projetandroid.adaptateurs.AdaptateurBieres;
import com.main.projetandroid.vue.VueMessage;

/**
 * Callback vérifiant si une page suivante est possible Si c’est le cas, le bouton suivant est
 * affiché.
 */
public class CallbackVerification implements FutureCallback<JsonArray> {
    protected final AdaptateurBieres bieres;
    protected final ImageButton bouton;
    protected final VueMessage message;
    protected int page;
    protected int nbResults;

    /**
     * Constructeur de la classe.
     *
     * @param boutonSuivant le bouton suivant de l’activité principale.
     */
    public CallbackVerification(AdaptateurBieres bieres, VueMessage message, ImageButton boutonSuivant) {
        this.bieres = bieres;
        this.bouton = boutonSuivant;
        this.message = message;
    }

    /**
     * Définit le nombre de résultats par page.
     *
     * @param nbResults Un entier supérieur à zéro.
     */
    public void setNbResults(int nbResults) {
        this.nbResults = nbResults;
    }

    /**
     * Définit la page en cours.
     *
     * @param page Un entier supérieur à zéro.
     */
    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public void onCompleted(Exception e, JsonArray result) {
        if (e == null) {
            if (result.size() > nbResults * page)
                bouton.setVisibility(View.VISIBLE);
            else
                bouton.setVisibility(View.GONE);
        } else {
            bieres.clear();
            message.erreurConnexion();
            Log.e("Recherche", e.getMessage());
        }
    }
}
