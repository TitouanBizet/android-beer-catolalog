package com.main.projetandroid.recherche;

/**
 * Exception lancée lorsque les valeurs de filtres choisies par l’utilisateur sont incorrectes.
 */
public class ExceptionParametres extends Exception {

}
