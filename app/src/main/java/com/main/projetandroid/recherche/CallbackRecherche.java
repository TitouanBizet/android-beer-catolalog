package com.main.projetandroid.recherche;

import android.util.Log;
import android.widget.ListView;

import com.google.gson.JsonArray;
import com.koushikdutta.async.future.FutureCallback;
import com.main.projetandroid.adaptateurs.AdaptateurBieres;
import com.main.projetandroid.modele.Bieres;
import com.main.projetandroid.vue.VueMessage;

/**
 * Contient une callback qui gère l’issue d’une requête Ion. Si elle est réussie, des bières sont
 * ajoutées à un ViewGroup.
 */
public class CallbackRecherche implements FutureCallback<JsonArray> {
    final protected AdaptateurBieres adaptateur;
    final protected VueMessage vueMessage;
    final protected ListView liste;

    /**
     * Crée une callback.
     *
     * @param adaptateur un adaptateur à remplir avec le résultat.
     * @param vueMessage une vue où afficher un message d’erreur.
     */
    public CallbackRecherche(AdaptateurBieres adaptateur, VueMessage vueMessage, ListView liste) {
        this.adaptateur = adaptateur;
        this.vueMessage = vueMessage;
        this.liste = liste;
    }

    @Override
    public void onCompleted(Exception e, JsonArray result) {
        if (e == null) {
            adaptateur.clear();

            if (result.size() > 0) {
                adaptateur.addAll(new Bieres(result));
                vueMessage.succes();
                liste.smoothScrollToPosition(0);
            } else {
                vueMessage.aucunResultat();
            }

        } else {
            vueMessage.erreurConnexion();

            Log.e("Recherche", e.getMessage());
        }
    }
}
