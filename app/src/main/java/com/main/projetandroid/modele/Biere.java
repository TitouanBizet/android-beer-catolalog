package com.main.projetandroid.modele;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Une bière décrite par un nom, une date, une description et l’URL d’une image.
 */
public class Biere implements Parcelable {
    public int id;
    @SerializedName("name")
    public String nom;
    @SerializedName("first_brewed")
    public String date;
    public float abv;
    public String description;
    @SerializedName("image_url")
    public String url_image;

    public Biere() {
    }

    protected Biere(Parcel in) {
        id = in.readInt();
        nom = in.readString();
        date = in.readString();
        abv = in.readFloat();
        description = in.readString();
        url_image = in.readString();
    }

    public static final Creator<Biere> CREATOR = new Creator<Biere>() {
        @Override
        public Biere createFromParcel(Parcel in) {
            return new Biere(in);
        }

        @Override
        public Biere[] newArray(int size) {
            return new Biere[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nom);
        dest.writeString(date);
        dest.writeFloat(abv);
        dest.writeString(description);
        dest.writeString(url_image);
    }
}
