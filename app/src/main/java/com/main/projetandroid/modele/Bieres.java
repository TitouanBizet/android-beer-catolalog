package com.main.projetandroid.modele;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Une liste de bières pouvant être instanciée avec un JsonArray.
 */
public class Bieres extends ArrayList<Biere> {

    /**
     * Crée une liste de Bières.
     */
    public Bieres() {
        super();
    }

    /**
     * Crée une liste de bières et la remplit.
     *
     * @param json un tableau json contenant les objets à ajouter.
     */
    public Bieres(JsonArray json) {
        super();

        Type type = new TypeToken<List<Biere>>() {}.getType();
        addAll(new Gson().fromJson(json, type));
    }
}
