package com.main.projetandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.main.projetandroid.modele.Biere;
import com.squareup.picasso.Picasso;

/**
 * Activité qui affiche le nom, la date, l’image et la description d’une bière préalablement ajoutée
 * en extra.
 */
public class ActiviteDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_biere);

        TextView nom = findViewById(R.id.nom);
        TextView dateAbv = findViewById(R.id.date_abv);
        ImageView image = findViewById(R.id.image);
        TextView description = findViewById(R.id.description);

        Biere biere = getIntent().getParcelableExtra("biere");

        nom.setText(biere.nom);
        dateAbv.setText(String.format("%s – %s°", biere.date, biere.abv).replaceAll("\\.0°", "°"));
        Picasso.get().load(biere.url_image).into(image);
        description.setText(biere.description);
    }
}