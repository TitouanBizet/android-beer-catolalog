# Android beer catolalog

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Description
I made this project for my second year of my computer science DUT (frenche degree equivalent in two years). The subject was the creation of an android app connected to the [punk API](https://punkapi.com/documentation/v2). Despite the app is translated in English, all code and documentation is in French. It uses some 

## Visuals
![image-1.png](./image-1.png)
![image-2.png](./image-2.png)

## Installation
The easiest way to install it is to use [Android Studio](https://developer.android.com/studio). You can also try to connect a device and to run ./gradlew installDebug in the root directory.

## Authors and acknowledgment
Thanks to Timothé Béziau--Jecty for his help and to Jean-François Remm for the courses.

## License
[CeCILL-B license](http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html)
